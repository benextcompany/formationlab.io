FROM node:lts

RUN mkdir /home/node/site
COPY . /home/node/site/

WORKDIR /home/node/site

RUN npm install
RUN npm install --save-dev node-sass
RUN npm run export

CMD ["npx", "serve", "./public"]