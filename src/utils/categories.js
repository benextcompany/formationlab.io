Array.prototype.groupBy = function(prop) {
  return this.reduce(function(groups, item) {
    const val = item[prop]
    groups[val] = groups[val] || []
    groups[val].push(item)
    return groups
  }, {})
}

const buildFormationsByCategories = formations => {
  return Object.entries( formations.groupBy('category') );
}

export { buildFormationsByCategories }
