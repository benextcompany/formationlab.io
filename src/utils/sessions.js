import { addDays, compareAsc, format, isAfter, parse } from 'date-fns';
import { fr } from 'date-fns/locale';

const processSessionsForFormation = (formation) => {
    const currentDate = new Date();

    formation.sessions = formation.dates
        .map(date => parse(date, 'yyyy/MM/dd', currentDate))
        .filter(parsedDate => isAfter(parsedDate, currentDate))
        .sort(compareAsc)
        .map(parsedDate => {
            let label = ''
            if (formation.duration == 1) {
                label = format(parsedDate, 'd MMMM yyyy', { locale: fr });
            } else {
                const firstDay = format(parsedDate, 'd MMMM', { locale: fr });
                const lastDay = format(addDays(parsedDate, formation.duration - 1), 'd MMMM', { locale: fr });
                const monthYear = format(parsedDate, 'yyyy');
                label = `${firstDay} au ${lastDay} ${monthYear}`;
            }
            return {
                label: label,
                isOnline: false,
                firstDay: parsedDate
            };
        });
    formation.nextsession = formation.sessions[0];

    if (formation.onlinedates) {
        formation.onlinesessions = formation.onlinedates
            .filter(dates => {
                let firstDate = parse(dates[0], 'yyyy/MM/dd', currentDate);
                return isAfter(firstDate, currentDate);
            })
            .map(dates => {
                let label = '';
                dates.forEach((date, index) => {
                    const parsedDate = parse(date, 'yyyy/MM/dd', currentDate);
                    const day = format(parsedDate, 'd');
                    const month = format(parsedDate, 'MMMM', { locale: fr });
                    if (index < dates.length - 1) {
                        label += `${day} ${month}, `;
                    } else {
                        const year = format(parsedDate, 'yyyy');
                        label += `${day} ${month} ${year}`;
                    }
                })
                return {
                    label: label,
                    isOnline: true,
                    firstDay: parse(dates[0], 'yyyy/MM/dd', currentDate)
                };
            });
        formation.nextonlinesession = formation.onlinesessions[0];
    }
}


const buildAllFormationSessions = (formation) => {
    processSessionsForFormation(formation);
    const sessions = formation.sessions ? formation.sessions : []
    const onlinesessions = formation.onlinesessions ? formation.onlinesessions : []
    const allSessions = sessions
        .concat(onlinesessions)
        .flat();
    allSessions.forEach(session => {
        session.title = formation.title;
        session.slug = formation.slug;
        session.color = formation.color;
    });
    return allSessions;
};

const buildSessions = (formations) => {
    return formations.flatMap(buildAllFormationSessions)
    .sort((a, b) => {
        return compareAsc(a.firstDay, b.firstDay);
    });
}

export { buildSessions, processSessionsForFormation }
