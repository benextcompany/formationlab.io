import formations from './_formation.js';
import { buildSessions } from '../utils/sessions.js'
import { buildFormationsByCategories } from '../utils/categories.js'

const response = {
	formationsByCategories: buildFormationsByCategories(formations.map(formation => {
		return {
			slug: formation.slug,
			title: formation.title,
			image: formation.image,
			description: formation.description,
			category: formation.category
		};
	})),
	sessions: buildSessions(formations)
}

export function get(req, res) {
	res.writeHead(200, {
		'Content-Type': 'application/json'
	});

	res.end(JSON.stringify(response));
}