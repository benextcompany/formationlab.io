import formations from './_formation.js';
import { processSessionsForFormation } from '../utils/sessions.js'

const formationMap = new Map();
formations.forEach(formation => {
	processSessionsForFormation(formation)
	formationMap.set(formation.slug, JSON.stringify(formation));
});

export function get(req, res, next) {
	// the `slug` parameter is available because
	// this file is called [slug].json.js
	const { slug } = req.params;

	if (formationMap.has(slug)) {
		res.writeHead(200, {
			'Content-Type': 'application/json'
		});

		res.end(formationMap.get(slug));
	} else {
		res.writeHead(404, {
			'Content-Type': 'application/json'
		});

		res.end(JSON.stringify({
			message: `Not found`
		}));
	}
}
