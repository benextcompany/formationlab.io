// Ordinarily, you'd generate this data from markdown files in your
// repo, or fetch them from a database of some kind. But in order to
// avoid unnecessary dependencies in the starter template, and in the
// service of obviousness, we're just going to leave it here.

// This file is called `_formations.js` rather than `formations.js`, because
// we don't want to create an `/blog/formations` route — the leading
// underscore tells Sapper not to do that.

const formations = [
	{
		"slug": "sensibilisation-agile",
		"title": "SENSIBILISATION AGILE",
		"category": "CULTURE AGILE",
		"imageauthor": "Rune Guneriussen",
		"description": "Cette formation d’une demi-journée propose une découverte rapide des principes agiles, expose ses bénéfices et impacts pour votre produit, pour vos équipes ; de basculer du paradigme projet au paradigme produit.",
		"meta": {
			"description": "Cette formation d’une demi-journée propose une découverte rapide des principes agiles, expose ses bénéfices et impacts pour votre produit, pour vos équipes ; de basculer du paradigme projet au paradigme produit.",
			"keywords": "formation, agile, culture, product, basics, mindset"
		},
		"mail": "benext%20-%20formation%20Sensibilisation%20Agile",
		"duration": 0.5,
		"price": "400 €",
		"dates": [],
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "Cette formation est constituée de <strong>70% d’ateliers</strong> , <strong>10% de théorie</strong> , <strong>20% d’échanges</strong>  et conversations entre les participants.",
			"block2": "Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables <br/> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Acculturation à l'état d'esprit agile, à l'essence de l'agilité",
			"block4": "Ateliers pratiques laissant la place à chacun de s'exprimer et d'apprendre à son rythme"
		},
		"learnings": [
			"<br/> Comprendre les <strong>origines</strong>, les <strong>avantages</strong> et les <strong>limites</strong> de l'agilité.",
			"<br/> Être sensibilisé au <strong>changement</strong> de paradigme de l’approche projet à l’<strong>approche produit</strong>.",
			"<br/> Comprendre l’importance d’une <strong>vision commune et partagée</strong>."
		],
		"programbydays": [
			{
				"title": "Programme de la demi-journée",
				"morning": {
					"title": "",
					"image": "",
					"description": "<br/> - Créer et partager une vision <br/> - La communication entre les différentes parties prenantes d’un produit <br/> - Mise en oeuvre des principes fondamentaux de l’agilité <br/> - Un peu de théorie agile"
				},
				"afternoon": {
					"title": "",
					"image": "",
					"description": ""
				}
			}
		],
		"forwho": {
			"description": "<br><em>Aucun prérequis n'est nécessaire, la formation sera plus bénéfique si vous êtes (ou si vous allez être à court terme) dans un environnement agile.</em>",
			"personas": [
				{
					"name": "Product owners débutants",
					"description": ""
				},
				{
					"name": "Scrum masters débutants",
					"description": ""
				},
				{
					"name": "Les acteurs du produit",
					"description": "Tous les acteurs du produit ayant déjà participés à la mise en oeuvre d’un produit et souhaitant s'immerger dans l’état d’esprit et la culture agile"
				},
				{
					"name": "Le grand public",
					"description": "A toute personne curieuse qui souhaite un tour d’horizon concis : chef de projet, directeur de projet, développeur, partie prenantes"
				}
			]
		}
	},
	{
		"slug": "agilite-et-scrum",
		"title": "AGILITÉ ET SCRUM",
		"category": "CULTURE AGILE",
		"imageauthor": "Rune Guneriussen",
		"description": "Découvrir en une journée les bénéfices et la mise en œuvre de l’agilité au travers du framework scrum.",
		"meta": {
			"description": "Découvrir en une journée les bénéfices et la mise en œuvre de l’agilité au travers du framework scrum.",
			"keywords": "formation, agile, culture, product, basics, mindset"
		},
		"mail": "benext%20-%20formation%20Agilite%20Et%20Scrum",
		"duration": 1,
		"price": "750 €",
		"dates": [],
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "Cette formation est constituée de <strong>60% d’ateliers</strong> , <strong>20% de théorie</strong> , <strong>20% d’échanges</strong>  et conversations entre les participants.",
			"block2": "Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables <br/> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Acculturation à l'état d'esprit agile, à l'essence de l'agilité",
			"block4": "Ateliers pratiques laissant la place à chacun de s'exprimer et d'apprendre à son rythme"
		},
		"learnings": [
			"<br/> Comprendre la <strong>force de l’agilité</strong> dans le monde complexe actuel",
			"<br/> Comprendre <strong>scrum</strong>",
			"<br/> Pouvoir <strong>contribuer efficacement</strong> à un produit en scrum"
		],
		"programbydays": [
			{
				"title": "",
				"morning": {
					"title": "Demi-journée 1",
					"image": "",
					"description": "<br/> - Bilan, état des lieux de votre situation projet - produit actuel <br/> - Pourquoi l’agilité ? <br/> - Scrum, un peu de théorie :<br/>   • l’essence de scrum <br/>   • les rôles <br/>    • les cérémonies <br/>   • les artefacts"
				},
				"afternoon": {
					"title": "Demi-journée 2",
					"image": "",
					"description": "<br/> - Atelier : mise en pratique de scrum <br/> - Lean coffee <br/> - Rétrospective"
				}
			}
		],
		"forwho": {
			"description": "<br><em>Aucun prérequis n'est nécessaire, la formation sera plus bénéfique si vous êtes (ou si vous allez être à court terme) dans un environnement agile.</em>",
			"personas": [
				{
					"name": "Product owners",
					"description": ""
				},
				{
					"name": "Scrum masters",
					"description": ""
				},
				{
					"name": "Les acteurs du produit",
					"description": "à toute personne impliquée dans la mise en œuvre d’un produit en scrum (chef de projet, directeur de projet, program manager, PMO, parties prenantes)"
				}
			]
		}
	},
	{
		"slug": "agilite-et-kanban",
		"title": "AGILITÉ ET KANBAN",
		"category": "CULTURE AGILE",
		"imageauthor": "Rune Guneriussen",
		"description": "Découvrir en une journée les bénéfices et la mise en œuvre de l’agilité au travers du framework kanban.",
		"meta": {
			"description": "Découvrir en une journée les bénéfices et la mise en œuvre de l’agilité au travers du framework kanban.",
			"keywords": "formation, agile, culture, product, basics, mindset"
		},
		"mail": "benext%20-%20formation%20Agilite%20Et%20Kanban",
		"duration": 1,
		"price": "750 €",
		"dates": [],
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "Cette formation est constituée de <strong>60% d’ateliers</strong> , <strong>20% de théorie</strong> , <strong>20% d’échanges</strong>  et conversations entre les participants.",
			"block2": "Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables <br/> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Acculturation à l'état d'esprit agile, à l'essence de l'agilité",
			"block4": "Ateliers pratiques laissant la place à chacun de s'exprimer et d'apprendre à son rythme"
		},
		"learnings": [
			"<br/> Comprendre la <strong>force de l’agilité</strong> dans le monde complexe actuel",
			"<br/> Comprendre <strong>kanban</strong>",
			"<br/> Pouvoir <strong>contribuer efficacement</strong> à un produit en scrum"
		],
		"programbydays": [
			{
				"title": "",
				"morning": {
					"title": "Demi-journée 1",
					"image": "",
					"description": "<br/> - Bilan, état des lieux de votre situation projet - produit actuel <br/> - Pourquoi l’agilité ? <br/> - Mise en pratique de kanban"
				},
				"afternoon": {
					"title": "Demi-journée 2",
					"image": "",
					"description": "<br/> - Kanban, un peu de théorie : <br/> • l’essence de kanban <br/> • le visuel management <br/> • les mesures <br/> - Lean coffee <br/> - Rétrospective"
				}
			}
		],
		"forwho": {
			"description": "<br><em>Aucun prérequis n'est nécessaire, la formation sera plus bénéfique si vous êtes (ou si vous allez être à court terme) dans un environnement agile.</em>",
			"personas": [
				{
					"name": "Product owners",
					"description": ""
				},
				{
					"name": "Kanbanmasters",
					"description": ""
				},
				{
					"name": "Les acteurs du produit",
					"description": "à toute personne impliquée dans la mise en œuvre d’un produit en scrum (chef de projet, directeur de projet, program manager, PMO, parties prenantes)"
				}
			]
		}
	},
	{
		"slug": "agilite-a-lechelle",
		"title": "AGILITÉ A L'ÉCHELLE",
		"category": "CULTURE AGILE",
		"imageauthor": "Rune Guneriussen",
		"description": "Découvrir en une journée les fondamentaux de l’agilité à l’échelle, ses bénéfices et sa mise en œuvre, les framework les plus couramment utilisés.",
		"meta": {
			"description": "Découvrir en une journée les fondamentaux de l’agilité à l’échelle, ses bénéfices et sa mise en œuvre, les framework les plus couramment utilisés.",
			"keywords": "formation, agile, culture, product, basics, mindset"
		},
		"mail": "benext%20-%20formation%20Agilite%20A%20Lechelle",
		"duration": 1,
		"price": "750 €",
		"dates": [],
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "Cette formation est constituée de <strong>60% d’ateliers</strong> , <strong>20% de théorie</strong> , <strong>20% d’échanges</strong>  et conversations entre les participants.",
			"block2": "Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables <br/> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Acculturation à l'état d'esprit agile, à l'essence de l'agilité",
			"block4": "Ateliers pratiques laissant la place à chacun de s'exprimer et d'apprendre à son rythme"
		},
		"learnings": [
			"<br/> Comprendre l’<strong>essence</strong> et les <strong>bénéfices</strong> de l'agilité à l’échelle, l’importance de la culture agile pour le passage à l’échelle",
			"<br/> Découvrir les <strong>fondamentaux de l’agilité à l’échelle</strong> au travers des frameworks les plus courants (SAFe, LeSS, Spotify, scrum de scrum)",
			"<br/> Appréhender les points forts et les écueils du passage à l’échelle"
		],
		"programbydays": [
			{
				"title": "",
				"morning": {
					"title": "Demi-journée 1",
					"image": "",
					"description": "<br/> - Quoi, quand et pourquoi l’agilité à l’échelle <br/> - Les frameworks les plus classiques : SAFe, LeSS, Spotify, scrum de scrum - points commun et divergences"
				},
				"afternoon": {
					"title": "Demi-journée 2",
					"image": "",
					"description": "<br/> - Cas pratique : atelier de synchronisation pluri-équipes <br/> - Lean coffee <br/> - Clôture"
				}
			}
		],
		"forwho": {
			"description": "<br><em>Aucun prérequis n'est nécessaire, la formation sera plus bénéfique si vous êtes (ou si vous allez être à court terme) dans un environnement agile.</em>",
			"personas": [
				{
					"name": "chef de projet, directeur de projet",
					"description": ""
				},
				{
					"name": "PMO, parties prenantes",
					"description": ""
				},
				{
					"name": "product owner, scrum master, encadrant d’équipe agile",
					"description": ""
				}
			]
		}
	},
	{
		"slug": "agile-product-ownership",
		"title": "LE PRODUIT I : AGILE PRODUCT OWNERSHIP",
		"category": "LE PRODUIT",
		"imageauthor": "Haracio Salinas",
		"description": "Découvrir en 2 jours le rôle du Product Owner (posture, responsabilités, droits et devoirs) et l’art de l’utilisation du product backlog, dans un environnement agile scrum.",
		"meta": {
			"description": "Découvrir en 2 jours le rôle du Product Owner (posture, responsabilités, droits et devoirs) et l’art de l’utilisation du product backlog, dans un environnement agile scrum.",
			"keywords": "formation, product owner, agile, product ownership",
		},
		"mail": "benext%20-%20formation%20agile%20product%20ownership",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		// one online session will be several dates, they are not necessarely consecutives
		"onlinedates": [
			["2021/10/11", "2021/10/12", "2021/10/14", "2021/10/15"],["2021/12/06", "2021/12/07", "2021/12/09", "2021/12/10"]
		],
		"how": {
			"description": "",
			"block1": "Cette formation est constituée de <strong>70% d’ateliers</strong> , <strong>10% de théorie</strong> , <strong>20% d’échanges</strong>  et conversations entre les participants.",
			"block2": "Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables <br/> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Acculturation à l’état d’esprit agile, à l’essence de l’agilité",
			"block4": "Ateliers pratiques sur des techniques efficaces de mises en oeuvre produit et “client centric”"
		},
		"learnings": [
			"Comprendre ce qu’est un product owner et ce qu’il n’est pas, son <strong>positionnement</strong> par rapport aux autres profils de l’équipe, aux parties prenantes, à tout l’écosystème produit.",

			" <br/> Maîtriser l’art et la manière de manier le <strong>product backlog</strong>, <strong>le sprint backlog</strong>, les <strong>user stories</strong>.",

			" <br/> Apprendre à <strong>découper</strong> de façon à décupler l’apprentissage métier (et technique) à <strong>moindre coût</strong> (financier et temporel).",

			" <br/> Apprendre à <strong>prioriser</strong> pour faire émerger la valeur métier essentielle, l’<strong>impact utilisateur</strong> dès les premières semaines de vie du produit.",

			" <br/> Découvrir la force d’une <strong>vision partagée</strong>."
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Demi-journée 1",
					"image": "agenda-1.jpg",
					"description": " <br/> - Attentes, état des lieux de votre situation produit actuelle <br/> - C’est quoi, vraiment, un product owner ? <br/> - Découpage et priorisation"
				},
				"afternoon": {
					"title": "Demi-journée 2",
					"image": "agenda-2.jpg",
					"description": " <br/> Le PO dans le cadre du framework agile scrum : <br/> - rôle et responsabilités associées <br/> - interactions et cérémonies <br/> - outils et artefacts"
				}
			},
			{
				"title": "Jour 2",
				"morning": {
					"title": "Demi-journée 3",
					"image": "agenda-3.jpg",
					"description": " <br/> - Le product backlog ? <br/> - Le sprint backlog <br/> - Les user story <br/> - Notions de fini"
				},
				"afternoon": {
					"title": "Demi-journée 4",
					"image": "agenda-4.jpg",
					"description": " <br/> - Et la vision ? Et la valeur ? <br/> - Lean coffee <br/> - Quizz <br/> - Rétrospective"
				}
			}
		],
		"forwho": {
			"description": "<em>Aucun prérequis n'est nécessaire, la formation sera plus bénéfique si vous êtes (ou si vous allez être à court terme) dans un environnement agile.</em>",
			"personas": [
				{
					"name": "Les product owners débutants",
					"description": "Cette formation est adaptée à tout product owner qui souhaite se former aux fondamentaux du métier"
				},
				{
					"name": "Les acteurs du produit",
					"description": "Tous les acteurs du produit (sponsors, développeurs, testeurs, ux et ui designers, etc) ayant déjà participés à la mise en oeuvre d’un produit et souhaitant s'immerger dans l’état d’esprit, la culture produit"
				},
				{
					"name": "Le grand public",
					"description": "Toute personne curieuse de découvrir ce rôle, ses enjeux et ses outils"
				}
			]
		},
		"testimonials": [
			{
				"name": "Bertrand BLAIS",
				"comment": "Un grand merci à Nils Lesieur & Loïc Le Molgat de chez benext pour les 2 jours de formation auprès de mes équipes. #minset #agile #testandlearn",
				"company": "KDS"
			},
			{
				"name": "Yannick LEMAINTEC",
				"comment": "Merci beaucoup, ces deux journées de formation ont été très instructives et enrichissantes. À bientôt !",
				"company": "Le Monde"
			},
			{
				"name": "Sylvain Caille",
				"comment": "Comme mes camarades, je te remercie pour cette belle formation. J’ai proposé à mon supérieur de tester la mise en place de certaines actions, j’ai hâte !",
				"company": "Groupe L'express.fr"
			},
			{
				"name": "Anais",
				"comment": "Merci Nils pour le support de formation ! Et encore merci pour la formation qui était vraiment top !",
				"company": "GRDF"
			}
		]
	},
	{
		"slug": "agile-product-design",
		"title": "LE PRODUIT II : IDÉATION - DESIGN SPRINT",
		"category": "LE PRODUIT",
		"imageauthor": "Alexandr Bognat",
		"description": "Vivre un design sprint en 2 jours. Comment, à partir d’une idée, ouvrir le monde des possibles puis converger vers une première solution métier. <br/> Découvrir comment s’articule le design sprint au sein d’une stratégie produit globale, comment passer de l’idée à la réalisation.",
		"meta": {
			"keywords": "formation, product cycle, agile, design sprint, MVP, backlog",
			"description": "Vivre un design sprint en 2 jours. Comment, à partir d’une idée, ouvrir le monde des possibles puis converger vers une première solution métier. <br/> Découvrir comment s’articule le design sprint au sein d’une stratégie produit globale, comment passer de l’idée à la réalisation.",
		},
		"mail": "benext%20-%20formation%20Agile%20Produc%20Design",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		// one online session will be several dates, they are not necessarely consecutives
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants.",
			"block2": "Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br/> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Acculturation à l’état d’esprit agile, à l’essence de l’agilité",
			"block4": "Ateliers pratiques sur des techniques efficaces de mises en oeuvre produit et “client centric”"
		},
		"learnings": [
			"<strong>Quand</strong> faire un Design sprint et <strong>comment</strong> le préparer ?",
			"<br/> <strong>Animer</strong> les étapes du design sprint.",
			"<br/> Animer des ateliers de <strong>génération d’idées</strong>.",
			"<br/> Confronter très tôt votre solution <strong>avec des utilisateurs</strong>.",
			"<br/> Comment bien enchaîner après un design sprint pour <strong>délivrer rapidement</strong> et éviter un essoufflement ?",
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Demi-journée 1 : Définir le vrai problème",
					"image": "agenda-1.jpg",
					"description": "<br/> - Le design Sprint : pourquoi, comment ? <br/> - Définition de votre challenge (sur un cas pratique) <br/> - Projection dans l’histoire vécue par l’utilisateur"
				},
				"afternoon": {
					"title": "Demi-journée 2 : Idéation et inspiration",
					"image": "agenda-2.jpg",
					"description": "<br/> - Techniques d’inspiration en groupe <br/> - Détection de la valeur le plus tôt possible <br/> Idéation individuelle en plusieurs temps"
				}
			},
			{
				"title": "Jour 2",
				"morning": {
					"title": "Demi-journée 3 : Convergence et prototypage",
					"image": "agenda-1.jpg",
					"description": "<br/> - Convergence vers une solution unique <br/> - Alignement sur la présentation du produit <br/> - Prototypage rapide <br/> - Préparation d’un test utilisateur"
				},
				"afternoon": {
					"title": "Demi-journée 4 : Comment enchaîner après un Design Sprint",
					"image": "agenda-2.jpg",
					"description": "<br/> - Validation de vos hypothèses <br/> - Comment enchaîner après un design sprint ? <br/> - Lean Coffee <br/> - Rétrospective"
				}
			}
		],
		"forwho": {
			"description": "Cette formation créée autour d’ateliers s’adresse à ceux qui travaillent ou désirent travailler avec une livraison itérative d’un produit. <br> <em>Aucun prérequis n'est nécessaire, elle sera plus bénéfique si vous êtes dans un environnement agile, dans une phase d’idéation.</em>",
			"personas": [
				{
					"name": "Sponsors",
					"description": "Permettre aux parties prenantes impliquées sur un nouveau produit de comprendre l’apport du design sprint et l’agilité à leur organisation "
				},
				{
					"name": "UX designers",
					"description": "Approfondir le design sprint"
				},
				{
					"name": "Product Owners",
					"description": "Appréhender la combinaison UX, design sprint et agilité"
				}
			]
		}
	},
	{
		"slug": "agile-product-management",
		"title": "LE PRODUIT III : PRODUCT MANAGEMENT",
		"category": "LE PRODUIT",
		"imageauthor": "Maiko Gubler",
		"description": "Parcourir en 2 jours le rôle et les enjeux du  product management tout au long de la vie d’un produit : discovery-cadrage, premiers sprints, suivi de production et améliorations, fin de vie.",
		"meta": {
			"keywords": "formation, product cycle, agile, product management, discovery, sprint, backlog",
			"description": "Parcourir en 2 jours le rôle et les enjeux du  product management tout au long de la vie d’un produit : discovery-cadrage, premiers sprints, suivi de production et améliorations, fin de vie.",
		},
		"mail": "benext%20-%20formation%20Product%20Management",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		// one online session will be several dates, they are not necessarely consecutives
		"onlinedates": [
			["2021/09/13", "2021/09/14", "2021/09/16", "2021/09/17"]
		],
		"how": {
			"description": "",
			"block1": "constituée de <strong>60% d’ateliers</strong>, <strong>20% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants.",
			"block2": "Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br/> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Acculturation à l’état d’esprit agile, à l’essence de l’agilité",
			"block4": "Ateliers pratiques sur des techniques efficaces de mises en oeuvre produit et “client centric”"
		},
		"learnings": [
			"<strong>Piloter, construire, améliorer, rendre ‘rentable’</strong> un produit tout au long de son cycle de vie",
			"<br/> Définir une <strong>vision</strong> et la décliner in fine en <strong>actions et impacts</strong>.",
			"<br/> Accorder la <strong>stratégie produit</strong> aux OKR d’entreprise.",
			"<br/> Gérer un produit <strong>multi-équipes</strong>",
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Demi-journée 1",
					"image": "agenda-1.jpg",
					"description": "<br/> - Forces et faiblesses de l’organisation actuelle <br/> - La vision : pourquoi ? Pour qui ?"
				},
				"afternoon": {
					"title": "Demi-journée 2",
					"image": "agenda-2.jpg",
					"description": "<br/> - OKR et vision <br/> - Décliner la vision : définir la stratégie <br/> - Décliner la vision : définir la tactique"
				}
			},
			{
				"title": "Jour 2",
				"morning": {
					"title": "Demi-journée 3",
					"image": "agenda-3.jpg",
					"description": "<br/> - Mise en oeuvre concrète <br/> - Construction et organisation des équipes en charge <br/> - Mesures et pilotage tout au long de la vie du produit"
				},
				"afternoon": {
					"title": "Demi-journée 4",
					"image": "agenda-4.jpg",
					"description": "<br/> - Stratégies de fin du produit <br/> - Lean coffee <br/> - Rétrospective"
				}
			}
		],
		"forwho": {
			"description": " <em> Pour suivre cette formation, il est nécessaire de connaître les bases du product ownership.</em> <br> Cette formation s'adresse aux :",
			"personas": [
				{
					"name": "Product Owners confirmés",
					"description": ""
				},
				{
					"name": "Product Managers",
					"description": ""
				},
				{
					"name": "Responsable de produit",
					"description": ""
				}
			]
		}
	},
	{
		"slug": "okr-maitriser-approche-par-impact",
		"title": "OKR I : MAÎTRISER L’APPROCHE PAR IMPACT",
		"category": "LE PRODUIT",
		"imageauthor": "Raj",
		"description": "L’enjeu de cette formation de 2 jours est de donner du sens à ce que l’on fait, veut faire, en tant qu’équipe, produit, métier. Et également d’avoir un alignement et une collaboration active de l’ensemble des acteurs pour l’atteinte des impacts attendus. <br> Qu’est-ce que signifie une approche par impact ? Quelle est la différence entre un minimum vital à avoir et une ambition à atteindre ? Comment aider mon équipe, service à travailler avec une approche par impact ? <br> Cette formation est basée sur le mindset de la méthode Objective Key Result (OKR). Elle vous permettra de savoir comment, à votre niveau, dans votre contexte professionnel, quel que soit votre périmètre de responsabilité, infuser un mindset d’approche par impact (le définir et le mettre en place). Par la pratique, vous vous appropriez la philosophie, en comprendrez les avantages afin de la tester à l’échelle de votre équipe/produit.",
		"meta": {
			"description": "L’enjeu de cette formation de 2 jours est de donner du sens à ce que l’on fait, veut faire, en tant qu’équipe, produit, métier. Et également d’avoir un alignement et une collaboration active de l’ensemble des acteurs pour l’atteinte des impacts attendus. <br> Qu’est-ce que signifie une approche par impact ? Quelle est la différence entre un minimum vital à avoir et une ambition à atteindre ? Comment aider mon équipe, service à travailler avec une approche par impact ? <br> Cette formation est basée sur le mindset de la méthode Objective Key Result (OKR). Elle vous permettra de savoir comment, à votre niveau, dans votre contexte professionnel, quel que soit votre périmètre de responsabilité, infuser un mindset d’approche par impact (le définir et le mettre en place). Par la pratique, vous vous appropriez la philosophie, en comprendrez les avantages afin de la tester à l’échelle de votre équipe/produit.",
			"keywords": "formation, produit, management, OKR",
		},
		"mail": "benext%20-%20formation%20okr1",
		"duration": 1,
		"price": "750€",
		"dates": [],
		"onlinedates": [
			["2021/10/04", "2021/10/05"],["2021/11/29", "2021/11/30"],
		],
		"how": {
			"description": "",
			"block1": "Adaptée, <strong>personnalisée</strong> en fonction du niveau de maturité et des attentes du groupe",
			"block2": "Constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Mise en pratique de la création d’objectifs et de critères de succès chiffrés",
			"block4": "Des discussions ouvertes pour permettre un partage et une projection dans son contexte"
		},
		"learnings": [
			"Maîtriser l’<strong>approche</strong> “ambition et clef du succès”",
			"<br/> Suivre et définir les actions à <strong>entreprendre</strong>",
			"<br/> Identifier les erreurs à ne pas commettre",
			"<br/> Identifier les éléments à <strong>mesurer</strong> et les actionner",
			"<br/> <strong>Communiquer</strong> efficacement sur l'avancement de l’impact"
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Demi-journée 1 : Ambitions c'est-à-dire?",
					"image": "",
					"description": "<br/> - Un peu d’histoire <br/> - Définir la notion d’impact <br/> - Poser les ambitions"
				},
				"afternoon": {
					"title": "Demi-journée 2 : Créons de l'impact",
					"image": "",
					"description": "<br/> - Définir les clefs du succès et les mesurer (inspiré de l’approche OKR) <br/> - Quelles ambitions fixer pour une période courte ?"
				}
			},
			{
				"title": "Jour 2",
				"morning": {
					"title": "Demi-journée 3 : Comment en décliner notre tactique",
					"image": "",
					"description": "<br/> - Identifier les contributions en cartographiant l’impact (Impact mapping) <br/> - Prioriser et se lancer"
				},
				"afternoon": {
					"title": "Demi-journée 4 : Dans la vraie vie",
					"image": "",
					"description": "<br/> - Gestion des premiers résultats et prise de décision <br/> - Lancement d’un nouveau cycle <br/> - Lean coffee et partage sur vos cas concrets <br/> - Clôture"
				}
			}
		],
		"forwho": {
			"description": "Cette formation s’adresse à tous ceux qui ont des responsabilités liées à la vision, la stratégie produit, mais surtout à sa mise en place et à son déroulement. Ceux qui souhaitent retrouver ou donner du sens, changer le prisme en place avec des éléments concrets. <br> <em> Pas de compétence technique particulière nécesaaire en prérequis, mais une véritable appétence à piloter différemment.</em>",
			"personas": [
				{
					"name": "Product Owner, Product Manager",
					"description": "Pour savoir implémenter une véritable approche produit afin de mieux partager la vision au sein des équipes"
				},
				{
					"name": "Product designer",
					"description": "Pour approfondir la démarche et la mettre en place pour aligner l’ensemble des produits de son portefeuille"
				},
				{
					"name": "Directeurs de projet",
					"description": "Pour permettre à leurs équipes de décliner la stratégie globale de manière pertinente au quotidien?"
				},
				{
					"name": "Manager de métier transverse (marketing, studio sales, RH)",
					"description": "Pour que vos ambitions et besoins soient traduits dans les produits que vous développez"
				}
			]
		},
		"testimonials": [
			{
				"name": "Laurent MORISSEAU",
				"comment": "Je te remercie pour le support et la formation. Les deux sont de grandes qualités. J'ai particulièrement apprécié ton expérience, ton implémentation produit de la méthode des OKRs et les ateliers de mise en pratique en fil conducteur.",
				"company": "Morisseau consulting"
			}
		]

	},
	{
		"slug": "okr-implementer-framework-ecosysteme",
		"title": "OKR II - IMPLÉMENTER LE FRAMEWORK OKR DANS VOTRE ÉCOSYSTÈME",
		"category": "LE PRODUIT",
		"imageauthor": "Raj",
		"description": "Cette formation de 2 jours est axée sur la méthode Objective Key Result (OKR). Elle permet, par la pratique, de s’approprier l’approche, d’en comprendre les avantages et les subtilités. Mais aussi de se questionner sur la manière de l'implémenter dans votre écosystème. <br> Elle a comme autre objectif d’apprendre à construire une dynamique propice à l’émergence d’une stratégie efficiente, d’aligner tous les acteurs autour d’ambitions et critères de succès communs, de co-construire des tactiques afin de valider les hypothèses.",
		"meta": {
			"description": "Cette formation de 2 jours est axée sur la méthode Objective Key Result (OKR). Elle permet, par la pratique, de s’approprier l’approche, d’en comprendre les avantages et les subtilités. Mais aussi de se questionner sur la manière de l'implémenter dans votre écosystème. <br> Elle a comme autre objectif d’apprendre à construire une dynamique propice à l’émergence d’une stratégie efficiente, d’aligner tous les acteurs autour d’ambitions et critères de succès communs, de co-construire des tactiques afin de valider les hypothèses.",
			"keywords": "formation, produit, management, OKR",
		},
		"mail": "benext%20-%20formation%20okr2",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		"onlinedates": [
			["2021/11/22", "2021/11/23", "2021/11/25", "2021/11/26"]
		],
		"how": {
			"description": "",
			"block1": "Adaptée, <strong>personnalisée</strong> en fonction du niveau de maturité et des attentes du groupe",
			"block2": "Constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Mise en pratique de la création d’objectifs et de critères de succès chiffrés",
			"block4": "Des discussions ouvertes pour permettre un partage et une projection dans son contexte"
		},
		"learnings": [
			"Co-construire les <strong>OKR stratégiques</strong>",
			"<br/> Décliner les <strong>OKR tactiques</strong>",
			"<br/> Déterminer les <strong>mesures</strong> vraiment pertinentes, comment les réaliser et les fiabiliser",
			"<br/> Comment mettre en place la <strong>méthodologie</strong> OKR dans votre écosystème ?"
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Demi-journée 1 : OKR stratégique",
					"image": "",
					"description": "<br/> - Un peu d’histoire <br/> - Construire les OKR stratégiques <br/>     - ambitions stratégiques <br/>     - critères de succès associés"
				},
				"afternoon": {
					"title": "Demi-journée 2 : OKR tactique part 1",
					"image": "",
					"description": "<br/> - Challenge et affinage des KRs (key results), alignement autour des critères de succès : donner un objectif chiffré, une durée, s’assurer d’avoir accès régulièrement et facilement aux données, clarifier le vocabulaire <br/> - Quelles ambitions fixer pour une période courte ?"
				}
			},
			{
				"title": "Jour 2",
				"morning": {
					"title": "Demi-journée 3 : OKR tactique part 2",
					"image": "",
					"description": "<br/> - Apprendre à décliner, de la stratégie à la tactique <br/> - Consolidation : critères de succès tactiques et mise en place sur suivi des critères de succès"
				},
				"afternoon": {
					"title": "Demi-journée 4 : Implémentation à l'échelle de l'entreprise",
					"image": "",
					"description": "<br/> - A l’échelle de l'écosystème <br/> - Lancement d’un nouveau cycle <br/> - Dans la vraie vie (facilitation) <br/> - Clôture"
				}
			}
		],
		"forwho": {
			"description": "Cette formation s’adresse à tous ceux qui ont des responsabilités liées à la vision, la stratégie produit, mais surtout à sa mise en place et à son déroulement. Ceux qui souhaitent retrouver ou donner du sens, changer le prisme en place avec des éléments concrets. <br> <em> Pas de compétence technique particulière nécesaaire en prérequis, mais une véritable appétence à piloter différemment.</em>",
			"personas": [
				{
					"name": "Product Owner confirmés",
					"description": ""
				},
				{
					"name": "Product managers et Directeurs de projet",
					"description": ""
				},
				{
					"name": "Head of product",
					"description": ""
				},
				{
					"name": "Responsables de produit souhaitant acquérir de nouveaux savoir faire",
					"description": ""
				}
			]
		}
	},
	{
		"slug": "passionate-product-leadership",
		"title": "PASSIONATE PRODUCT LEADERSHIP avec Jeff Patton",
		"category": "LE PRODUIT",
		"imageauthor": "A PROPOS DU FORMATEUR<br><br>Jeff Patton travaille dans le développement de logiciels depuis plus de 25 ans. Il a commencé à travailler avec l'Extreme Programming en 2000. Le terme Agile a été inventé en 2001 et Jeff a appris alors qu’il utilisait un processus Agile et sa carte de visite indiquait 'chef de produit'. Bien qu’ayant occupé presque tous les rôles dans le développement de logiciels, la direction de produit est celui sur lequel il s’est toujours concentré.<br><br>Jeff a appris au cours des 20 dernières années qu'une bonne réflexion sur les produits nécessite un mélange de réflexion business, de gestion de produits, de conception de l'expérience utilisateur et de solides pratiques d'ingénierie. Elle ne peut être réalisée que dans le cadre d'un effort de collaboration. Et il fusionne toutes ces disciplines du mieux qu’il peut dans son enseignement.<br><br>Jeff Patton est connu pour ses nombreuses interventions publiques et pour avoir écrit le livre User Story Mapping.",
		"description": "<br>Pendant deux journées complètes, <strong>Jeff Patton</strong> viendra animer sa formation sur la conception des produits technologiques d’aujourd’hui, le leadership et les processus associés. Cette formation certifiante (CSPO) aborde la complémentarité de l’approche produit avec le développement agile et Scrum. Au programme, différentes mises en pratique collaboratives et ateliers participatifs, aux échanges très enrichis par l’expérience de son illustre formateur.<br><br> Une approche solide centrée sur le produit met l'accent sur les clients et les utilisateurs et sur la création de résultats fructueux et, en fin de compte, d'un impact important sur votre entreprise. Tout cela semble bien, voire évident, non ? Mais, dans votre organisation, vous vous concentrez peut-être davantage sur la satisfaction des parties prenantes et la livraison dans les délais que sur la réussite des produits. Ce n'est pas que ces choses ne soient pas importantes mais le fait de s'y attacher principalement peut vous empêcher, vous et votre équipe, de vous concentrer réellement sur les résultats qui profitent à votre organisation.<br><br>Cette formation est <strong>dispensée en anglais</strong>, la maitrise de l'anglais est fortement recommandée.",
		"meta": {
			"description": "Pendant deux journées complètes, <strong>Jeff Patton</strong> viendra animer sa formation sur la conception des produits technologiques d’aujourd’hui, le leadership et les processus associés. Cette formation certifiante (CSPO) aborde la complémentarité de l’approche produit avec le développement agile et Scrum. Au programme, différentes mises en pratique collaboratives et ateliers participatifs, aux échanges très enrichis par l’expérience de son illustre formateur.<br><br> Une approche solide centrée sur le produit met l'accent sur les clients et les utilisateurs et sur la création de résultats fructueux et, en fin de compte, d'un impact important sur votre entreprise. Tout cela semble bien, voire évident, non ? Mais, dans votre organisation, vous vous concentrez peut-être davantage sur la satisfaction des parties prenantes et la livraison dans les délais que sur la réussite des produits. Ce n'est pas que ces choses ne soient pas importantes mais le fait de s'y attacher principalement peut vous empêcher, vous et votre équipe, de vous concentrer réellement sur les résultats qui profitent à votre organisation.<br><br>Cette formation est <strong>dispensée en anglais</strong>, la maitrise de l'anglais est fortement recommandée.",
			"keywords": "formation, produit, management, leadership",
		},
		//formation speciale animée par qqn d'autre
		"specialformation": "avec Jeff Patton",
		//url d'inscription
		"mail": "https://www.eventbrite.com/e/in-person-passionate-product-leadership-workshop-paris-france-tickets-158493214471",
		"duration": 2,
		"price": "1349€",
		"dates": ["2021/09/22"],
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "Adaptée, <strong>personnalisée</strong> en fonction du niveau de maturité et des attentes du groupe",
			"block2": "Constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> entre les participants et l'animateur. <br> Un livret personnalisé et récapitulatif des ateliers et des apprentissages sera distribué à l’issue de la formation.",
			"block3": "- Une volonté de mettre en avant <strong>la mise en pratique</strong>, très pragmatique et focalisée sur des exercices dynamiques. Les apprentissages sont immédiatement applicables. <br><br> - Une large place aux <strong>discussions ouvertes</strong> pour permettre un <strong>partage</strong> et une projection dans son contexte",
			"block4": "Si vous êtes Product owner ou Product manager, vous savez probablement déjà que vous aurez le plus de succès si vous <strong>collaborez efficacement avec toute votre équipe</strong>, et si toute votre équipe comprend <strong>l’approche produit</strong>. <br><br>Cette formation vous aidera à mieux comprendre l’approche produit et les méthodes de travail qui la soutiennent. <strong>Jeff Patton</strong> s'appuie sur les principes agiles et la pratique de Scrum et y ajoute son expertise de l’approche produit que vous n'obtiendrez pas avec le développement agile seul. Vous repartirez avec un état d'esprit qui vous aidera à aider d'autres personnes dans votre organisation, ainsi que des pratiques qui vous aideront à faire votre travail au quotidien. <br><br><strong>Ce que vous verrez et ferez pendant l'atelier :</strong> <br><strong>Jeff Patton</strong> animera ses ateliers en utilisant un mélange de discussion, de présentation traditionnelle, et beaucoup de dessins à la main en direct pour soutenir les discussions. Si vous avez déjà assisté à une de ses conférences, vous savez à quoi cela ressemble. C'est beaucoup plus facile de se concentrer et d’apprendre ainsi qu’avec des bullets Powerpoint et des slides.  <br><br> A travers de ses sessions de travail, <strong>Jeff Patton</strong> évoquera des approches à superposer au développement agile et au cadre Scrum. Des choses comme :<br> - Lean Startup et la pratique Lean UX<br> - La réflexion sur le design<br> - Le développement à double voie<br> - User story et Story Mapping<br><br> Il sera fait en sorte que les groupes de travail en équipe soient petits et dynamiques, idéalement de 3 à 5 personnes. Vous aurez beaucoup d’opportunités d’échanges et de conversations avec les autres participants. Idéalement, vous aurez l'occasion de rencontrer et de parler avec tous les participants de la formation.<br><br>De courtes pauses fréquentes seront proposées pour que vous puissiez rester concentrés. <br><br><strong>Ce que vous recevrez à l'issue de la formation : </strong><br><br> - Des feuilles de travail, des articles et un guide de cours de 120 pages : Ce support vous aidera à vous rappeler et à mettre en pratique tout ce que vous aure abordé pendant la formation.<br><br> - Une certification Scrum Alliance : il s’agit de la formation <strong>Certified Scrum Product Owner (CSPO)</strong>, donc à la fin de la formation - si vous avez assisté à l'ensemble des sessions - vous recevrez la certification CSPO de la Scrum Alliance ainsi que l'adhésion à la Scrum Alliance.<br><br> - Repas et collations servis chaque jour : petit-déjeuner léger, déjeuner, collations du matin et de l'après-midi et quelques boissons."
		},
		"learnings": [
			"Évaluer dans quelle mesure votre processus est aujourd'hui <strong>centré sur le produit</strong>",
			"<br/> Organiser les équipes autour de vos produits",
			"<br/> Comprendre les rôles essentiels du <strong>leadership</strong> au sein d'une équipe produit",
			"<br/> Faire évoluer les équipes de produits au sein de grandes organisations complexes",
			"<br/> Construire une <strong>compréhension partagée</strong> de vos produits actuels à l'aide de proto-personas et de cartes de parcours simples",
			"<br/> Identifier les bons indicateurs de produit",
			"<br/> Rédiger des OKR utiles qui aideront votre équipe à <strong>établir des priorités</strong> et à planifier ses activités",
			"<br/> Organiser les roadmap de produits et les plans stratégiques en fonction des résultats business",
			"<br/> Mener le travail de découverte des produits en utilisant des hypothèses et des tests",
			"<br/> Créer des <strong>stratégies de lancement de produits efficaces</strong> qui mettent l'accent sur l'apprentissage et la formation à chaque lancement",
			"<br/> Créer des stratégies de développement efficaces qui mettent l'accent sur la prédictibilité et la qualité",
			"<br/> Travaillez avec votre équipe pour l'aider à planifier et à gérer tactiquement chaque sprint",
			"<br/> Intégrer le discovery continu et la livraison continue dans le développement à double voie"
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Principes fondamentaux du développement de produits",
					"image": "",
					"description": "<br/> - Visualiser la production, les résultats et l'impact <br/> - Comprendre et évaluer l'ensemble du cycle de vie du développement de produits <br/>     - Comprendre les principales menaces pour les processus centrés sur les produits <br/>     - Organiser les équipes autour de vos produits"
				},
				"afternoon": {
					"title": "Écouter, apprendre et se concentrer",
					"image": "",
					"description": "<br/> - User stories et Story essentials <br/> - Story Mapping <br/> - Proto-personas et cartes de parcours <br/> - Identifier les problèmes et les opportunités du produit <br/> - Mesures du produit et indicateurs clés de performance (KPI) <br/> - Utilisation de la vision, de la stratégie et des OKR pour cibler les priorités "
				}
			},
			{
				"title": "Jour 2",
				"morning": {
					"title": "Découverte du produit",
					"image": "",
					"description": "<br/> - Évaluer et hiérarchiser les opportunités <br/> - Transformer les idées en hypothèses <br/> - Identifier les hypothèses et les tests risqués <br/> - Impliquer l'ensemble de l'équipe dans les activités de découverte <br/> - Product discovery continu et livraison continue grâce au dual-track développement "
				},
				"afternoon": {
					"title": "Release Planning et livraison du produit",
					"image": "",
					"description": "<br/> - Décomposer les fonctionnalités à l'aide d'une carte narrative <br/> - Création d'une stratégie de livraison incrémentale <br/> - Identification des releases pour apprendre et des releases pour obtenir des bénéfices <br/> - Création de stratégies de développement axées sur la prédictibilité et la qualité <br/> - Collaborer avec l'équipe à chaque sprint"
				}
			}
		],
		"forwho": {
			"description": "Cette formation s’adresse à tous ceux qui ont des responsabilités liées à la stratégie produit et sa conception, mais aussi à l'organisation que cela demande et sa mise en place jusque son déroulement. <em> Pas de compétence technique particulière nécessaire en prérequis, mais une véritable appétence à piloter différemment.</em>",
			"personas": [
				{
					"name": "Product owners, Product managers, Head of product, Chief product officer",
					"description": "Si vous êtes Product owners, Product managers, head of product, Chief Product officer, cet atelier est pour vous. Mais pas seulement !"
				},
				{
					"name": "Designers, UX, ergonomes",
					"description": "Vous savez probablement déjà que les meilleures décisions en matière de produits sont celles qui concilient les préoccupations commerciales, l'expérience utilisateur et la technologie. Si vous êtes un praticien de l'UX ou un ingénieur senior, cet atelier est pour vous"
				},
				{
					"name": "Scrum master, coach agile",
					"description": "Si vous êtes un Scrum master ou un coach agile, avez-vous vu votre organisation lutter pour appliquer l’approche produit en utilisant une approche agile ? Si vous souhaitez mieux comprendre comment aider votre organisation à devenir une organisation produit forte, cet atelier est pour vous"
				},
				{
					"name": "Partie prenante, dirigeant, leader",
					"description": "Si vous êtes une partie prenante, un gestionnaire ou un leader dans votre organisation, comprenez-vous comment l’approche produit change la façon dont vous devriez travailler avec les équipes ? "
				},
				{
					"name": "autre acteurs clé de l'organisation",
					"description": "Si vous souhaitez mieux comprendre comment motiver les équipes et les garder concentrées sur des résultats fructueux tout en étant auto-organisées, cet atelier est pour vous."
				}
			]
		}
	},
	{
		"slug": "scrum-master",
		"title": "COACHING D'ÉQUIPE I : DÉCOUVRIR LE RÔLE SCRUM MASTER",
		"category": "FACILITATION ET COACHING",
		"imageauthor": "Olgaç Bozalp",
		"description": "L’objectif de cette formation est de comprendre ce qu’apporte l’agilité dans une équipe et sa mise en application par Scrum en particulier. Elle donne les éléments de base pour se lancer dans l’accompagnement d’une équipe au quotidien avec Scrum. <br> Dans sa version à distance, elle permet d’appréhender des outils collaboratifs et la facilitation en visio-conférence.",
		"meta": {
			"description": "L’objectif de cette formation est de comprendre ce qu’apporte l’agilité dans une équipe et sa mise en application par Scrum en particulier. Elle donne les éléments de base pour se lancer dans l’accompagnement d’une équipe au quotidien avec Scrum. <br> Dans sa version à distance, elle permet d’appréhender des outils collaboratifs et la facilitation en visio-conférence.",
			"keywords": "formation, scrum, agile, scrum master",
		},
		"mail": "benext%20-%20formation%20coaching%20equipe1",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		"onlinedates": [
			["2021/09/27", "2021/09/28", "2021/09/30", "2021/10/01"]
		],
		"how": {
			"description": "",
			"block1": "Adaptée, <strong>personnalisée</strong> en fonction du niveau de maturité et des attentes du groupe",
			"block2": "Constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Apprendre à accompagner son équipe dans sa vie quotidienne avec Scrum avec des exercices pratiques",
			"block4": "Développer sa posture de facilitateur avec des jeux de rôle"
		},
		"learnings": [
			"<br/> Comprendre les <strong>bénéfices</strong> de l’agilité et de scrum",
			"<br/> Découvrir les principaux <strong>rituels</strong> de scrum",
			"<br/> <strong>Faciliter</strong> les rituels et le travail de l’équipe",
			"<br/> Développer la <strong>posture</strong> de facilitateur",
			"<br/> Découvrir des <strong>ateliers</strong> agiles afin d’aider l’équipe",
			"<br/> Utiliser des outils de <strong>facilitation à distance</strong>"

		],
		"programbydays": [
			{
				"title": "Jour 1 : Agile et Scrum",
				"morning": {
					"title": "Demi-journée 1",
					"image": "",
					"description": "<br> - Pourquoi être agile? Pourquoi fonctionner en Scrum? <br> - Expérimenter les avantages d’être agile: découper et prioriser. <br> - Comment le manifeste agile est né et ce qu’il nous dit."
				},
				"afternoon": {
					"title": "Demi-journée 2",
					"image": "",
					"description": "<br> - Comment une équipe Scrum planifie et délivre de la valeur? <br> - Vivre la vie d’une équipe Scrum sur quelques itérations <br> - Découvrir par la pratique les rôles dans l’équipe Scrum"
				}
			},
			{
				"title": "Jour 2 : Facilitation et accompagnement quotidien",
				"morning": {
					"title": "Demi-journée 3",
					"image": "",
					"description": "<br> - Quels indicateurs suivre pour rendre visible l’avancement de l’équipe.? <br> - Identifier les rôles et responsabilités de chacun. <br> - Faciliter le daily meeting de l’équipe par le jeu de rôle"
				},
				"afternoon": {
					"title": "Demi-journée 4",
					"image": "",
					"description": "<br> - Comment amener l’équipe à s’améliorer en continu? <br> - Préparer et animer une rétrospective par le jeu de rôle <br> - Bilan de la formation"
				}
			}
		],
		"forwho": {
			"description": "Pour celles et ceux qui veulent débuter dans le rôle de scrum master et apprendre à accompagner une équipe agile. <br> <em>Aucun prérequis n'est nécessaire. Débutants, agilistes et non agilistes bienvenus.</em>",
			"personas": [
				{
					"name": "A tous les acteurs du produit",
					"description": "Cette formation s’adresse aux scrum masters, aux chefs de projet, aux directeurs de projets, aux membres d’une équipe agile qui souhaitent comprendre son rôle, ses enjeux et ses outils dans un contexte agile"
				}
			]
		}
	},
	{
		"slug": "perfectionner-sa-posture",
		"title": "COACHING D'ÉQUIPE II : PERFECTIONNER SA POSTURE",
		"category": "FACILITATION ET COACHING",
		"imageauthor": "Katarzyna Pe",
		"description": "Cette formation propose d’aller au-delà du rôle de scrummaster à l’aide d’outils collaboratifs favorisant l’entraide et l’introspection. Développer son leadership afin d’accompagner, d’aider et soutenir son équipe en travaillant la posture de facilitateur d’équipe, inter-équipe ou de produit (scrum master, manager, product owner).",
		"meta": {
			"description": "Cette formation propose d’aller au-delà du rôle de scrummaster à l’aide d’outils collaboratifs favorisant l’entraide et l’introspection. Développer son leadership afin d’accompagner, d’aider et soutenir son équipe en travaillant la posture de facilitateur d’équipe, inter-équipe ou de produit (scrum master, manager, product owner).",
			"keywords": "formation, scrum, agile, scrum master",
		},
		"mail": "benext%20-%20formation%20coaching%20equipe2",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		"onlinedates": [
			["2021/12/13", "2021/12/14", "2021/12/16", "2021/12/17"]
		],
		"how": {
			"description": "",
			"block1": "Adaptée, <strong>personnalisée</strong> en fonction du niveau de maturité et des attentes du groupe",
			"block2": "Constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Aider à développer son équipe en expérimentant des ateliers collaboratifs entre pairs",
			"block4": "Aider à développer sa posture par des moments d’introspection et d’enrichissement personnels"
		},
		"learnings": [
			"<br/> Identifier le <strong>rôle</strong> dont a vraiment besoin votre organisation",
			"<br/> <strong>Déclencher</strong> des conversations inspirantes",
			"<br/> Développer votre <strong>posture</strong> de facilitateur d’équipe",
			"<br/> Proposer du <strong>management visuel</strong> et l’animer",
			"<br/> <strong>Faciliter</strong> les échanges, s’adapter en permanence",
			"<br/> Animer des pratiques de <strong>développement personnel et collectif</strong>",
			"<br/> Entrevoir la richesse de l’<strong>introspection</strong> dans l’atteinte de ses objectifs",
			"<br/> <strong>Sensibiliser</strong> votre organisation à la recherche d’excellence",
			"<br/> Les participants travaillent des compétences telles que l’écoute active, l’humilité, la remise en question, l’apprentissage ou le leadership autant d’atouts pour développer le métier de Scrum Master ou de facilitateur."

		],
		"programbydays": [
			{
				"title": "Jour 1 : Accompagnement d’équipe",
				"morning": {
					"title": "Demi-journée 1",
					"image": "agenda-1.jpg",
					"description": "<br> - Envisager votre rôle sous un jour nouveau <br> - Où se situer dans son parcours agile ? Explorer de nouveaux chemins"
				},
				"afternoon": {
					"title": "Demi-journée 2",
					"image": "agenda-2.jpg",
					"description": "<br> - Limiter le travail en cours pour fluidifier le processus d’une équipe <br> - Rendre son travail visible pour piloter son activité <br> - Prioriser en équipe et rapidement, en finir avec les débats sans fin"
				}
			},
			{
				"title": "Jour 2 : Introspection & posture",
				"morning": {
					"title": "Demi-journée 3",
					"image": "agenda-3.jpg",
					"description": "<br> - Vivre la mêlée de l’intérieur via le jeu de rôles <br> - Développer son potentiel de facilitation, s’adapter à son auditoire pour avoir plus d’impact"
				},
				"afternoon": {
					"title": "Demi-journée 4",
					"image": "agenda-4.jpg",
					"description": "<br> - Identifier ses forces, trouver son “flow” pour relever tous ses défis <br> - Partager ses expériences et développer son potentiel grâce à la force du groupe <br> - L’art de la rétrospective"
				}
			}
		],
		"forwho": {
			"description": "Cette formation s'adresse à ceux qui veulent jouer un rôle dans un contexte de transformation agile et grandir sur l’accompagnement d’équipe. <br> <em> La formation sera plus bénéfique si vous avez une ou plusieurs expériences d’un rôle de scrum master ou de facilitateur de projets</em>",
			"personas": [
				{
					"name": " Scrum masters",
					"description": "Cette formation s’adresse aux scrum masters ayant déjà une expérience."
				},
				{
					"name": "Coachs agiles",
					"description": ""
				},
				{
					"name": "Managers & Directeurs de projets",
					"description": ""
				}
			]
		},
		"testimonials": [
			{
				"name": "Charles",
				"comment": "Une formation très utile et intéressante, beaucoup d’énergie, en espérant mettre tout à profit. Merci beaucoup",
				"company": "Fujitsu"
			},
			{
				"name": "Yannick LEMAINTEC",
				"comment": "Good vibes, bonne énergie, lieu convivial, je repars pleine de nouvelles idées",
				"company": "MMB"
			},
			{
				"name": "Lucie",
				"comment": "Beaucoup d’énergie et de bonne humeur durant cette formation ! Trop courte, la session pourrait durer bien plus longtemps. De belles rencontres. ",
				"company": "Fujitsu"
			},
			{
				"name": "Padja",
				"comment": "Format d’échange hyper chouette. De belles rencontres et une bonne énergie. Ça fait du bien !",
				"company": "Meetic"
			}
		]
	},
	{
		"slug": "coaching-organisation",
		"title": "COACHING ORGANISATION I : DE L’ÉQUIPE À L’ORGANISATION",
		"category": "FACILITATION ET COACHING",
		"imageauthor": "Katarzyna Pe",
		"description": "L’objectif de cette formation est de décrypter et de comprendre le rôle et la raison d’être d’un coach en organisation.",
		"meta": {
			"description": "L’objectif de cette formation est de décrypter et de comprendre le rôle et la raison d’être d’un coach en organisation.",
			"keywords": "formation, scrum, agile, coach, scrum master",
		},
		"mail": "benext%20-%20formation%20coaching%20organisation1",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "Adaptée, <strong>personnalisée</strong> en fonction du niveau de maturité et des attentes du groupe",
			"block2": "Constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Aider à développer son équipe en expérimentant des ateliers collaboratifs entre pairs",
			"block4": "Aider à développer sa posture par des moments d’introspection et d’enrichissement personnels"
		},
		"learnings": [
			"<br/> Découvrir le rôle de <strong>coach</strong> en entreprise",
			"<br/> Développer le <strong>leadership</strong> qui sommeille en vous",
			"<br/> Coaching, facilitation, mentoring, formation, danser avec les postures",
			"<br/> Découvrir les principes de l'<strong>invitation</strong>, un levier de l'engagement et de la performance",
			"<br/> Comprendre ce qui se joue dans les <strong>dynamiques</strong> de groupe",
			"<br/> Les participants travaillent des compétences telles que l’écoute active, l’humilité, la remise en question, l’apprentissage ou le leadership autant d’atouts pour développer le métier de Scrum Master ou de facilitateur."
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Demi-journée 1 : POSTURE",
					"image": "",
					"description": "<br> - Le rôle de coach en entreprise est parfois difficile à cerner. <br> - Découvrir les différentes postures d’un rôle à travers une grille de lecture simple. <br> - Par l’évaluation de son jeu de pratiques, découvrir son leadership"
				},
				"afternoon": {
					"title": "Demi-journée 2: COACHING",
					"image": "",
					"description": "<br> - Découvrir le coaching à travers la pyramide DILTS. <br> - Découvrir un outil de coaching individuel et organisationnel."
				}
			},
			{
				"title": "Jour 2",
				"morning": {
					"title": "Demi-journée 3 : INVITATION",
					"image": "",
					"description": "<br> - Découvrir les principes et les étapes menant à la performance. <br> - Trouver le juste équilibre entre adhésion et efficacité des équipes."
				},
				"afternoon": {
					"title": "Demi-journée 4 : FACILITATION",
					"image": "",
					"description": "<br> - Découvrir la palette du facilitateur. <br> - Danser avec les postures, s’adapter au groupe en permanence"
				}
			}
		],
		"forwho": {
			"description": "Cette formation s'adresse à ceux qui veulent jouer un rôle dans un contexte de transformation agile et grandir sur l’accompagnement d’équipe. <br> <em> La formation sera plus bénéfique si vous avez une ou plusieurs expériences d’un rôle de scrum master ou de facilitateur de projets</em>",
			"personas": [
				{
					"name": " Scrum masters expérimentés",
					"description": "Cette formation s’adresse aux scrum masters ayant déjà une expérience."
				},
				{
					"name": "Coachs agiles, coachs interne, agents du changement",
					"description": ""
				},
				{
					"name": "Managers & Directeurs de projets",
					"description": ""
				}
			]
		}
	},
		{
		"slug": "coaching-organisation",
		"title": "COACHING ORGANISATION II : DEVENIR UNE ENTREPRISE AGILE DANS UN MONDE COMPLEXE",
		"category": "FACILITATION ET COACHING",
		"imageauthor": "Katarzyna Pe",
		"description": "Les nouveaux contextes amènent une nouvelle façon de penser (agile) qui amène de nouvelles approches, et des nouvelles façons de s'organiser, de penser et de bâtir les entreprises.",
		"meta": {
			"description": "Les nouveaux contextes amènent une nouvelle façon de penser (agile) qui amène de nouvelles approches, et des nouvelles façons de s'organiser, de penser et de bâtir les entreprises.",
			"keywords": "formation, organisation, agile, coach, dirigeants",
		},
		"mail": "benext%20-%20formation%20coaching%20organisation2",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "Adaptée, <strong>personnalisée</strong> en fonction du niveau de maturité et des attentes du groupe",
			"block2": "Constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Aider à développer son équipe en expérimentant des ateliers collaboratifs entre pairs",
			"block4": "Aider à développer sa posture par des moments d’introspection et d’enrichissement personnels"
		},
		"learnings": [
			"<br/> Comprendre les cycles de vie et les typologies des entreprises.",
			"<br/> Comprendre les principes qui régissent la structuration des entreprises dites agiles.",
			"<br/> Les chemins vers l'entreprise agile dans un monde complexe.",
			"<br/> Comment opérer une transformation."
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Demi-journée 1 : ETAT DES LIEUX",
					"image": "",
					"description": "<br> - Le cycle de vie des entreprises et leur typologie. <br> - La structuration physique des entreprises (taille des départements, régularité des rituels, organisation produit, etc.). <br> - Les types d'entreprises agiles, l'entreprise agile, et l'entreprise agile à l'échelle. <br> - Les écueils, les risques, les points de vigilance."
				},
				"afternoon": {
					"title": "Demi-journée 2: REVISION",
					"image": "",
					"description": "<br> - Révision des attentes de l'entreprise : cheminement des employés. <br> - Révision de l'organisation de l'entreprise : comment revenir à une approche plus organique. <br> - Comment opérer une transformation. Étapes, risques et cheminement."
				}
			},
		],
		"forwho": {
			"description": "Cette formation s'adresse à ceux qui veulent jouer un rôle dans un contexte de transformation organistionnelle. <br> <em> La formation sera plus bénéfique si vous avez une ou plusieurs expériences d’un rôle de dirigeant, d'accompagnant au sein de votre organisation</em>",
			"personas": [
				{
					"name": " Scrum masters et coachs expérimentés",
					"description": "Cette formation s’adresse à des rôles d'accompagnement ayant déjà une expérience."
				},
				{
					"name": "Dirigeants, Managers & Directeurs de projets",
					"description": "Cette formation s’adresse à ceux qui veulent déclencher le changement en organisation."
				}
			]
		}
	},
	{
		"slug": "apprendre-a-dire-les-choses",
		"title": "APPRENDRE A DIRE LES CHOSES",
		"category": "LEADERSHIP",
		"imageauthor": "John Yuyi",
		"description": "Apprendre à dire les choses est une formation sur la posture de bon communicant au service d'une personne ou d'un collectif. Les participants travaillent des compétences telles que l’écoute active, l’humilité, les modèles de feedback, l'intelligence émotionnelle afin de développer une attitude adaptée quel que soit le message à passer.",
		"meta": {
			"description": "Apprendre à dire les choses est une formation sur la posture de bon communicant au service d'une personne ou d'un collectif. Les participants travaillent des compétences telles que l’écoute active, l’humilité, les modèles de feedback, l'intelligence émotionnelle afin de développer une attitude adaptée quel que soit le message à passer.",
			"keywords": "formation, intelligence émotionnelle, feedback, communiquer, oser",
		},
		"mail": "benext%20-%20formation%20Apprendre%20à%20dire%20les%20choses",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		"onlinedates": [
			["2021/11/08", "2021/11/09", "2021/11/11", "2021/11/12"]
		],
		"how": {
			"description": "",
			"block1": "Adaptée et personnalisée en fonction des attentes et de la maturité du groupe",
			"block2": "Constituée de <strong>60% d’ateliers</strong>, <strong>20% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Aider à développer son équipe en expérimentant des ateliers collaboratifs et des moments de partage",
			"block4": "Aider à développer sa posture par des moments d’introspection et d’enrichissement personnels"
		},
		"learnings": [
			"<strong>Adapter</strong> sa communication au profil de son interlocuteur",
			"Renforcer sa <strong>capacité de persuasion</strong> pour donner de l'<strong>impact</strong> à ses idées",
			"Développer un <strong>comportement assertif</strong> dans ses relations interpersonnelles ",
			"Découvrir l'<strong>intelligence émotionnelle</strong>",
			"Apprendre à <strong>composer avec les émotions</strong> des autres personnes",
			"Oser avoir des <strong>conversations difficiles</strong>"
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Demi-journée 1",
					"description": "<br> - Comprendre les besoins et entrevoir ceux des autres. <br> - Apprendre à vous exprimer de manière limpide et à faire passer les bons messages, au bon moment avec le canal de communication approprié. <br> - Une sensibilisation à la grille de lecture Processus Communication sera un outil utile."
				},
				"afternoon": {
					"title": "Demi-journée 2",
					"description": "<br> - Faire la différence entre émotions et sentiments <br> - Comprendre comment notre corps fait circuler nos émotions <br> - Nos peurs sont-elles fondées ? <br> - Utiliser l’intelligence émotionnelle pour mieux comprendre et mieux interagir avec un interlocuteur ou interlocutrice."
				}
			},
			{
				"title": "Jour 2",
				"morning": {
					"title": "Demi-journée 3",
					"description": "<br> - Découvrir plusieurs techniques de dialogues en one to one <br> - Savoir mener une discussion importante : résolution de conflits et impacts <br> - Apprendre à faire et recevoir un feedback <br> - Découvrir la Communication Non Violente, une des bases indispensables pour passer un message constructif."
				},
				"afternoon": {
					"title": "Demi-journée 4",
					"description": "<br> - Quelles sont les techniques de dialogues en collectif ? <br> - Nous utiliserons ici des techniques d’improvisation théâtrales <br> - Comment mener des discussions collectives permettant de prendre des actions concrètes"
				}
			}
		],
		"forwho": {
			"description": "Pour celles et ceux qui ont des interactions régulières avec une posture d'accompagnateur. Développer le potentiel de chacun dans l'expression de ses besoins et partager des messages difficiles de façon audible pour autrui.",
			"personas": [
				{
					"name": "Manager",
					"description": "Oser avoir une conversation difficile"
				},
				{
					"name": "RH",
					"description": "Adapter son discours en fonction de son interlocuteur"
				},
				{
					"name": "Scrum Masters & Coachs agiles",
					"description": "Travailler votre posture de communiquant"
				}
			]
		}
	},
	{
		"slug": "apprendre-a-pitcher",
		"title": "APPRENDRE A PITCHER",
		"category": "LEADERSHIP",
		"imageauthor": "John Yuyi",
		"description": "Apprendre à pitcher est une formation sur la création d’un pitch de présentation produit et sur la posture de bon communicant. Les participants apprennent à se servir d’outils tels que le Lean Canvas, un outil focalisé sur la proposition de valeur ou bien le pitch elevator un outil efficace de communication. Cette formation a pour objectif de travailler également la posture et la pose de la voix qui ensemble donnent toute la force et l’impact voulus au discours.",
		"meta": {
			"description": "Apprendre à pitcher est une formation sur la création d’un pitch de présentation produit et sur la posture de bon communicant. Les participants apprennent à se servir d’outils tels que le Lean Canvas, un outil focalisé sur la proposition de valeur ou bien le pitch elevator un outil efficace de communication. Cette formation a pour objectif de travailler également la posture et la pose de la voix qui ensemble donnent toute la force et l’impact voulus au discours.",
			"keywords": "formation, intelligence émotionnelle, feedback, communiquer, oser",
		},
		"mail": "benext%20-%20formation%20Apprendre%20à%20pitcher",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "Adaptée et personnalisée en fonction des attentes et de la maturité du groupe",
			"block2": "Constituée de <strong>60% d’ateliers</strong>, <strong>20% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Aider à développer son équipe en expérimentant des ateliers collaboratifs et des moments de partage",
			"block4": "Aider à développer sa posture par des moments d’introspection et d’enrichissement personnels"
		},
		"learnings": [
			"<br/> Apprendre à <strong>parler de son produit</strong>",
			"<br/> <strong>Construire</strong> sa trame de discours",
			"<br/> Apprendre à <strong>lâcher prise</strong>",
			"<br/> Apprendre à <strong>poser sa voix</strong>"
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Demi-journée 1",
					"description": "<br> - Présentation du Lean canvas. <br> - Quels sont les 3 principaux problèmes que vous souhaitez résoudre. Qui sont vos clients ? Peuvent-ils être segmentés ?"
				},
				"afternoon": {
					"title": "Demi-journée 2",
					"description": "<br> - En quoi votre offre répond-elle efficacement aux besoins du marché ? En quoi est elle différente et meilleure que les autres ?  <br> - Quelles sont les 3 principales solutions apportées par votre offre pour répondre aux problèmes ou aux besoins de vos clients ?"
				}
			},
			{
				"title": "Jour 2",
				"morning": {
					"title": "Demi-journée 3",
					"description": "<br> - Lâcher prise, apprendre à poser sa voix <br> - Scripting : formaliser le “quoi ?” <br> - Trouver la trame d’un bon pitch"
				},
				"afternoon": {
					"title": "Demi-journée 4",
					"description": "<br> - Passer de l’écrit à l’oral <br> - Techniques de respiration <br> - Boucle de feedback, répétitions <br> - Enregistrement vidéo"
				}
			}
		],
		"forwho": {
			"description": "Pour celles et ceux qui veulent promouvoir leurs idées, produits ou service.",
			"personas": [
				{
					"name": "Entrepreneurs, intrapreneurs, porteurs d’innovation",
					"description": ""
				},
				{
					"name": "Le grand public",
					"description": ""
				}
			]
		}
	},
	{
		"slug": "management-agile",
		"title": "MANAGEMENT AGILE I : DU MANAGER AU LEADER",
		"category": "LEADERSHIP",
		"imageauthor": "John Yuyi",
		"description": "Dans un monde de plus en plus complexe, il est primordial de cultiver de nouvelles formes de management et de leadership pour dépasser les modes de gestion classiques devenus souvent des freins.",
		"meta": {
			"description": "Dans un monde de plus en plus complexe, il est primordial de cultiver de nouvelles formes de management et de leadership pour dépasser les modes de gestion classiques devenus souvent des freins.",
			"keywords": "formation, agile, transformation, coaching",
		},
		"mail": "benext%20-%20formation%20Management%20à%20Agile1",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		"onlinedates": [
			["2021/11/15", "2021/11/16", "2021/11/18", "2021/11/19"]
		],
		"how": {
			"description": "",
			"block1": "Adaptée et personnalisée en fonction des attentes et de la maturité du groupe",
			"block2": "Constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Aider à développer son équipe en expérimentant des ateliers collaboratifs et des moments de partage",
			"block4": "Aider à développer sa posture par des moments d’introspection et d’enrichissement personnels"
		},
		"learnings": [
			"<br/> Changer la <strong>posture du manager</strong>",
			"<br/> Comprendre comment la complexité affecte votre organisation",
			"<br/> Appréhender les <strong>nouvelles postures</strong> du manager leader/coach : porter la vision, gérer des conflits et des problèmes",
			"<br/> Découvrir votre potentiel de coach avec le <strong>solution focus</strong>",
			"<br/> S'inspirer d’un type de leadership avec l’<strong>host leadership</strong>",
			"<br/> <strong>Agir avec les équipes</strong> : Fédérer, décider ensemble et déléguer"
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Demi-journée 1: Vers le leadership Agile",
					"description": "<br> - Découvrir les différents types de leadership <br> - Comprendre le leadership agile <br> - Connaître les ingrédients de l’engagement des équipes agiles"
				},
				"afternoon": {
					"title": "Demi-journée 2: Qu’est ce qui motive vos équipes ?",
					"description": "<br> - Apprendre à connaître la différence entre les motivations extrinsèques et les motivations intrinsèques, les désirs intrinsèques et les techniques communes pour comprendre ce qui est important pour les membres de vos équipes, telles que les rencontres face à face, des évaluations personnelles ou les questions les plus importantes (moving motivators) <br> - Comprendre vos besoins et entrevoir ceux des autres. <br> - Apprendre à vous exprimer de manière limpide et à faire passer les bons messages, au bon moment avec le canal de communication approprié. <br> - Une sensibilisation à la grille de lecture Processus Communication sera un outil utile."
				}
			},
			{
				"title": "Jour 2",
				"morning": {
					"title": "Demi-journée 3: Développer le potentiel et l’autonomie de vos équipes",
					"description": "<br> - Découvrir un pattern de coaching basé sur Solution focus pour aider vos collaborateurs à développer leurs savoir-faire et leur savoir-être <br> - Redéfinir votre management et impliquer votre équipe grâce au delegation board permettant de définir collectivement les rôles et périmètres de décision"
				},
				"afternoon": {
					"title": "Demi-journée 4: Vers le management agile",
					"description": "<br> - Apprendre comment construire l’auto-organisation, renforcer la responsabilisation et adapter votre dispositif à travers une approche agile tirée du management visuel. <br> - Apprendre à connaître les 4 facettes de la conduite du changement qui s’appliquent au système, aux individus, aux interactions et à la limite du système"
				}
			}
		],
		"forwho": {
			"description": "Cette formation s'adresse à ceux qui veulent jouer un rôle dans un contexte de transformation agile ou organisationnelle. <br> <em> La formation sera plus bénéfique si vous connaissez les principes et pratiques agiles, si vous êtes scrum master/coachs avec de l'expérience ou si vous faite partie d'un service de conduite du changemen ou de transformation.</em>",
			"personas": [
				{
					"name": "Coachs Agiles & Scrum Masters",
					"description": ""
				},
				{
					"name": "Consultants en conduite du changement",
					"description": ""
				},
				{
					"name": "Managers, dirigeants",
					"description": ""
				},
			]
		}
	},
		{
		"slug": "management-agile",
		"title": "MANAGEMENT AGILE II : PILOTER DANS UN MONDE COMPLEXE",
		"category": "LEADERSHIP",
		"imageauthor": "John Yuyi",
		"description": "Les nouveaux contextes amènent une nouvelle façon de penser (agile) qui amène de nouvelles approches, et une nouvelle façon de voir et de vivre le rôle du manager. Comment appréhender cette nouvelle posture, la comprendre, et se l'approprier.",
		"meta": {
			"description": "Les nouveaux contextes amènent une nouvelle façon de penser (agile) qui amène de nouvelles approches, et une nouvelle façon de voir et de vivre le rôle du manager. Comment appréhender cette nouvelle posture, la comprendre, et se l'approprier.",
			"keywords": "formation, agile, transformation, coaching, management",
		},
		"mail": "benext%20-%20formation%20Management%20à%20Agile2",
		"duration": 1,
		"price": "1500€",
		"dates": [],
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "Adaptée et personnalisée en fonction des attentes et de la maturité du groupe",
			"block2": "Constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "Aider à développer son équipe en expérimentant des ateliers collaboratifs et des moments de partage",
			"block4": "Aider à développer sa posture par des moments d’introspection et d’enrichissement personnels"
		},
		"learnings": [
			"<br/> Comprendre comment la complexité affecte le rôle du manager",
			"<br/> Comprendre comment appréhender votre nouveau rôle de manager",
			"<br/> S'entraîner et se projeter avec de nouveaux outils",
			"<br/> Comprendre les contextes et les postures du manager agile : Cynefin"
		],
		"programbydays": [
			{
				"title": "Jour 1",
				"morning": {
					"title": "Demi-journée 1: COMPLEXITE",
					"description": "<br> - Comprendre les contextes dans lesquels vous évoluez et comment les appréhender. <br> - Comprendre pourquoi la majorité des problèmes sont désormais complexes et ce que cela apporte comme changement dans le rôle du manager. <br> - Comprendre pourquoi l'engagement des collaborateurs est devenu clef."
				},
				"afternoon": {
					"title": "Demi-journée 2: POSTURE",
					"description": "<br> - Projection sur une approche <strong>hôte</strong> : Atelier d'amélioration de vos pratiques. <br> - Prioriser et déléguer pour prendre de meilleures décisions : Ateliers de transition issus du management 3.0"
				}
			},
		],
		"forwho": {
			"description": "Cette formation s'adresse à ceux qui veulent jouer un rôle dans un contexte de transformation agile ou organisationnelle. <br> <em> La formation sera plus bénéfique si vous connaissez les principes et pratiques agiles, si vous êtes scrum master/coachs avec de l'expérience ou si vous faite partie d'un service de conduite du changemen ou de transformation.</em>",
			"personas": [
				{
					"name": "Coachs Agiles & Scrum Masters",
					"description": ""
				},
				{
					"name": "Consultants en conduite du changement",
					"description": ""
				},
				{
					"name": "Managers, dirigeants",
					"description": ""
				},
			]
		}
	},
	{
		"slug": "qualite-du-code-guide-par-les-tests",
		"title": "DÉVELOPPEUR I : AMÉLIORER LA QUALITÉ DE VOTRE CODE PAR LE TEST	",
		"category": "Développement IT",
		"imageauthor": "Randy Fath",
		"description": "L'objectif de cette formation est de découvrir et ancrer les pratiques et concepts qui permettent d'améliorer la qualité du code. Avoir du code de qualité c'est permettre la livraison rapide et continue de valeur métier en production mais aussi faciliter l'évolution du logiciel. Venez apprendre à créer du code propre guidé par les tests, un design centré sur le métier ainsi que les principes de la programmation objet et fonctionnelle.<br>'Du code de qualité guidé par les tests' est une formation sur le développement de logiciel construite autour de la pratique. <br/> Les participants et le formateur travailleront en groupe afin de grandir collectivement.",
		"meta": {
			"description": "Pour tous ceux qui délivrent régulièrement du code, venez grandir sur le code propre guidé par les tests et centré sur le métier en mob programming.",
			"keywords": "formation, code, kata, test driven development, mob programming"
		},
		"mail": "benext%20-%20formation%20qualite%20de%20code",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "Adaptée, <strong>personnalisée</strong> en fonction du niveau de maturité et des attentes du groupe",
			"block2": "Constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "<strong>Mise en pratique</strong> de la création de code propre guidé par les tests",
			"block4": "Apprentissage par l'ensemble du groupe grace à la pratique d'un <strong>“mob programming“</strong>"
		},
		"learnings": [
			"<br/> Comprendre la philosophie de l’artisanat logiciel",
			"<br/> Découvrir le <strong>mob programming</strong>",
			"<br/> Se sentir à l'aise avec le <strong>Test Driven Development</strong>",
			"<br/> <strong>Projeter</strong> correctement le domaine métier dans votre code",
			"<br/> Comprendre l'émergence du <strong>design guidé par les tests</strong>",
			"<br/> Créer des <strong>architectures propres</strong> facilitant les tests",
			"<br/> Les principes fondamentaux de la <strong>programmation objet</strong> comme SOLID",
			"<br/> Des principes de la <strong>programmation fonctionnelle</strong>"
		],
		"programbydays": [
			{
				"title": "Jour 1 : Découvrir les tests et le TDD",
				"morning": {
					"title": "Introduction aux tests et aux pratiques de code propre",
					"image": "agenda-1.jpg",
					"description": "<br> - Vous découvrirez comment tester votre code. <br> - Utilisation du langage du métier et tester des cas d’utilisation. <br> - Comment contrôler les effets de bord et séparer le métier de l’infrastructure technologique ?"
				},
				"afternoon": {
					"title": "Le TDD comme approche du design du code",
					"image": "agenda-2.jpg",
					"description": "<br> - Comment découper un problème ? Quels tests à quel endroit ? Outside-in vs Inside-out. <br> - Pourquoi TDD nous aide à faire émerger le design du code <br> - Pourquoi les tests nous permettent de modifier le code en tout sécurité"
				}
			},
			{
				"title": "Jour 2 : Consolidation et tests sur du legacy",
				"morning": {
					"title": "Consolidation du TDD et pratique de tests plus avancées",
					"image": "agenda-1.jpg",
					"description": "<br> - Mise en pratique plus libre par le groupe pour consolider les acquis de la première journée <br> - Principes de code propre et d’organisation <br> - Découverte de pratique de tests plus avancées"
				},
				"afternoon": {
					"title": "Comment tester et reprendre en main du code legacy",
					"image": "agenda-2.jpg",
					"description": "<br> - Comment reprendre en main du code legacy en introduisant des tests ? <br> - Questions / Réponses et ouverture vers des sujets comme le DDD ou la programmation fonctionnelle."
				}
			}
		],
		"forwho": {
			"description": "A tous ceux qui produisent régulièrement du code dans le cadre de leur rôle. Les développeurs bien sûr mais aussi les data scientists et les data engineers. <br> <em>Aucun prérequis n'est nécessaire. Le choix du langage dépendra du groupe et il n'y a pas besoin de prérequis sur le langage choisi.</em>",
			"personas": [
				{
					"name": "Développeurs",
					"description": "Java/Kotlin - Javascript/Typescript - Swift"
				},
				{
					"name": "Data scientists",
					"description": "Python"
				},
				{
					"name": "Data engineers",
					"description": "Scala"
				}
			]
		},
		"testimonials": [
			{
				"name": "Jan Van Dravik",
				"comment": "Le kata, une discipline indispensable. Merci à Yannick pour l’accompagnement des équipes de dev scala de la tribu data. Une montée en puissance graduelle autour des fondamentaux du code tels qu’ils se pratiquent aujourd’hui.",
				"company": "BNP Paribas"
			},
			{
				"name": "Hugo Drivière",
				"comment": "J'ai trouvé l'expérience particulièrement bénéfique. Par la pratique, j'ai découvert le développement TDD et force est de constater que l'essayer c'est l'adopter. De plus, en réalisant ces différents katas en mob programming nous avons pu challenger nos idées et être bien plus efficace.",
				"company": "Vyve"
			},
			{
				"name": "Allan Jard",
				"comment": "J'ai participé à une série de leçons pratiques sur le TDD enseignées par Yannick. Pas de présentation ou de diaporama, simplement un IDE et votre langage de programmation préféré. Plus qu'une technique, j'ai appris un état d'esprit qui m'aide à être un ingénieur logiciel plus complet. ",
				"company": "Didomi"
			}
		]
	},
	{
		"slug": "du-metier-au-code",
		"title": "DÉVELOPPEUR II : DU MÉTIER AU CODE",
		"category": "Développement IT",
		"imageauthor": "Erwin Redl",
		"description": "L'objectif de cette formation est d'éviter la dette fonctionnelle et maîtriser la transition du besoin métier à la solution technique. Vous y découvrirez comment mieux analyser votre domaine métier, le partager et le projeter correctement dans le code.<br>'Du métier au code' est une formation sur le développement de logiciel construite autour de la pratique. <br/> Les participants et le formateur travailleront en groupe afin de grandir collectivement",
		"meta": {
			"description": "Pour tous ceux qui ceux qui veulent avoir un logiciel qui représente correctement le besoin métier",
			"keywords": "formation, code, Domain Driven Design, métier, compréhension du besoin"
		},
		"mail": "benext%20-%20formation%20du%20métier%20au%20code",
		"duration": 2,
		"price": "1500€",
		"dates": [],
		"onlinedates": [
			[]
		],
		"how": {
			"description": "",
			"block1": "Adaptée, <strong>personnalisée</strong> en fonction du niveau de maturité et des attentes du groupe",
			"block2": "Constituée de <strong>70% d’ateliers</strong>, <strong>10% de théorie</strong>, <strong>20% d’échanges</strong> et conversations entre les participants. Très pragmatique et focalisée sur des mises en pratique, les apprentissages sont immédiatement applicables. <br> Un livret personnalisé récapitulatif des ateliers et des apprentissages est distribué à l’issue de la formation.",
			"block3": "<strong>Mise en situation</strong> sur des ateliers de co-compréhension et partage de l'expertise métier",
			"block4": "Apprentissage par la mise en pratique des techniques permettant de relier <strong>métier et code</strong>"
		},
		"learnings": [
			"<br/> <strong> Mieux comprendre</strong> les domaines et le métier",
			"<br/> <strong>Mieux partager</strong> la connaissance métier",
			"<br/> Découvrir le <strong>design guidé par le métier (DDD)</strong>",
			"<br/> Utiliser des outils comme l'<strong>Event Storming</strong>",
			"<br/> <strong>Projeter</strong> correctement le métier dans votre code",
			"<br/> Apprendre à écrire des tests decrivant le métier (BBD)",
			"<br/> Apprendre à spécifier par l'exemple et créer une documentation vivante",
			"<br/> Utiliser des méthodes comme l'<strong>example mapping</strong> pour mieux partager le besoin"
		],
		"programbydays": [
			{
				"title": "Jour 1 : Mieux comprendre et partager la connaissance métier",
				"morning": {
					"title": "Demi-journée 1",
					"image": "",
					"description": "<br> - Comprendre et partager les différents domaines d'un système et leurs interactions en utilisant une carte du contexte."
				},
				"afternoon": {
					"title": "Demi-journée 2",
					"image": "",
					"description": "<br> - Comprendre et partager plus précisement un domaine, en particulier ses processus métiers, avec l'event storming."
				}
			},
			{
				"title": "Jour 2 : Décrire et projeter correctement le métier dans le code",
				"morning": {
					"title": "Demi-journée 3",
					"image": "",
					"description": "<br> - Spécifier les User Stories par des exemples en utilisant l'example mapping."
				},
				"afternoon": {
					"title": "Demi-journée 4",
					"image": "",
					"description": "<br> - Relier cette spécification au code par l'intermediaire du BDD pour créer une documentation vivante."
				}
			}
		],
		"forwho": {
			"description": "A tous ceux qui veulent s'assurer que le code qui part en prod correspond bien au besoin métier",
			"personas": [
				{
					"name": "PO, BA",
					"description": "Pour mieux décrire et partager le besoin"
				},
				{
					"name": "Dev",
					"description": "Pour écrire de code propre répondant correctement au besoin"
				},
				{
					"name": "QA",
					"description": "Pour écrire de bons tests"
				}
			]
		}
	},
];

formations.forEach(formation => {
	//formation.html = formation.html.replace(/^\t{3}/gm, '');
});

export default formations;
