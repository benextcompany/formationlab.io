# site de formations beNext (avec 'server side rendering')

Techno utilisée :
[Sapper](https://github.com/sveltejs/sapper)  
Plus d'info sur [sapper.svelte.dev](https://sapper.svelte.dev) pour démarrer.
Avec SCSS pris en compte (cf [article medium](https://medium.com/@sean_27490/svelte-sapper-with-sass-271fff662da9) et plugin [Svelte Preprocess](https://github.com/kaisermann/svelte-preprocess))

### Tester en local

Installer les dépendances puis lancer le mode dev avec rechargement à chaud

```bash
npm install # or yarn
npm run dev
```

Puis ouvrez [localhost:3000](http://localhost:3000)


Vous pouvez tester l'export pour le site statique avec
```bash
npm run export
```
et voir les résultats dans ./public/

### Ajouter et modifier une formation

L'ensemble des textes et dates des formations est dans le fichier *./src/routes/_formation.js* (format json)  
Pour modifier le contenu:
- modifier le fichier *_formation.js* 
- commit et push la modification (directement sur gitlab.com ou depuis votre installation locale)

Il y a du déploiement continu en place, donc quand le commit est push sur la branche master, ça déploit directement en production sur  [formation.benextcompany.com/](https://formation.benextcompany.com/).

Pour voir le statut de votre mise en production, voir les [pipelines sur gitlab](https://gitlab.com/benextcompany/formation.gitlab.io/-/pipelines).    

Les images des pages formation sont à rajouter dans le répertoire *./static/{formation.slug}*

*formation.slug* correspond à la dernière partie de l'URL de la formation, par exemple "agile-product-ownership" pour la formation Agile Product Ownership

Pour ajouter une formation, il faut ajouter son contenu dans .*/src/routes/_formation.js* et ajouter un dossier avec ses images dans *./static*. Le nom du dossier doit correspondre au *slug* de la formation défini dans *_formation.js*

Les images doivent être au format jpeg et porter les noms suivants:
- *intro.jpg* : image affichée avec l'introduction sur la page formation
- *opengraph.jpg* : image affichée quand on partage le lien vers la page formation ou dans un moteur de recherche (en général on met la même que *intro.jpg*)
- *agenda-1.jpg* : image de la demi-journée 1 du programme
- *agenda-2.jpg* : image de la demi-journée 2 du programme
- *agenda-3.jpg* : image de la demi-journée 3 du programme
- *agenda-4.jpg* : image de la demi-journée 4 du programme

Les images sont optionnelles, les pages formation fonctionnent aussi sans.
 





